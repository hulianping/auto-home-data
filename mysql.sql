﻿/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50715
 Source Host           : localhost:3306
 Source Schema         : qiche

 Target Server Type    : MySQL
 Target Server Version : 50715
 File Encoding         : 65001

 Date: 03/05/2021 00:24:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yoshop_qc_brand
-- ----------------------------
DROP TABLE IF EXISTS `yoshop_qc_brand`;
CREATE TABLE `yoshop_qc_brand` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '品牌id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '品牌名称',
  `first_letter` varchar(2) NOT NULL DEFAULT '' COMMENT '名称首字母',
  `logo` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'logo',
  `country` varchar(50) NOT NULL DEFAULT '' COMMENT '国家',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='车辆品牌表';

-- ----------------------------
-- Table structure for yoshop_qc_info
-- ----------------------------
DROP TABLE IF EXISTS `yoshop_qc_info`;
CREATE TABLE `yoshop_qc_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_valid` tinyint(1) unsigned DEFAULT '1',
  `model_id` int(11) DEFAULT NULL COMMENT 'model_id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '车型描述',
  `edition` varchar(255) NOT NULL DEFAULT '' COMMENT '版本',
  `car_model` varchar(255) NOT NULL DEFAULT '' COMMENT '车型 0.SUV 1.SRV 2.CRV 3.CUV 4.RV 5.HRV 6.RAV 7.MPV 9.NCV',
  `displacement` varchar(20) NOT NULL DEFAULT '' COMMENT '排量',
  `seating` int(11) NOT NULL DEFAULT '0' COMMENT '座位数',
  `drive` varchar(20) NOT NULL DEFAULT '' COMMENT '驱动方式 0.前驱 1.后驱 2.四驱',
  `energy` varchar(50) NOT NULL DEFAULT '' COMMENT '能源',
  `price` varchar(50) NOT NULL DEFAULT '' COMMENT '价格描述',
  `level_info` varchar(20) NOT NULL DEFAULT '' COMMENT '级别',
  `length` int(11) NOT NULL DEFAULT '0' COMMENT '长度（mm）',
  `width` int(11) NOT NULL DEFAULT '0' COMMENT '宽度（mm）',
  `height` int(11) NOT NULL DEFAULT '0' COMMENT '高度',
  `wheelbase` int(11) NOT NULL DEFAULT '0' COMMENT '轴距（mm）',
  `highest_km` int(11) NOT NULL DEFAULT '0' COMMENT '最高车速KM/H',
  `tank` int(11) NOT NULL DEFAULT '0' COMMENT '油箱容积L',
  `cylinder` int(11) NOT NULL DEFAULT '0' COMMENT '气缸数（个）',
  `engine_type` varchar(255) NOT NULL DEFAULT '' COMMENT '发动机型号',
  `horsepower` int(11) NOT NULL DEFAULT '0' COMMENT '最大马力（ps）',
  `power` int(11) NOT NULL DEFAULT '0' COMMENT '最大功率（kw）',
  `speed` varchar(255) NOT NULL DEFAULT '' COMMENT '最大功率转速(rpm)',
  `torque` int(11) NOT NULL DEFAULT '0' COMMENT '最大扭矩(N·m)',
  `torque_speed` varchar(255) NOT NULL DEFAULT '' COMMENT '最大扭矩转速(rpm)',
  `oil_wear` varchar(50) NOT NULL DEFAULT '' COMMENT '油耗',
  `list_time` varchar(20) DEFAULT NULL COMMENT '上市时间',
  `stop_time` varchar(20) DEFAULT NULL COMMENT '停产时间',
  `remarks` text COMMENT '备注',
  `created_at` int(11) DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5536 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for yoshop_qc_model
-- ----------------------------
DROP TABLE IF EXISTS `yoshop_qc_model`;
CREATE TABLE `yoshop_qc_model` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '型号id',
  `brand_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '品牌id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '型号名称',
  `first_letter` varchar(2) NOT NULL DEFAULT '' COMMENT '名称首字母',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '默认未拉取详细信息 1.已拉取',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='车辆型号表';

SET FOREIGN_KEY_CHECKS = 1;
